import React from "react";
import './Buscador.css';

function Buscador({searchValue, setSearchValue}){
    const onSearchValueChange = (event) => {
        console.log(event.target.value);
        setSearchValue(event.target.value);
    } 
    return(
        <input className="Buscador" placeholder="Buscar una tarea" value={searchValue} onChange={onSearchValueChange}/>
    );
}

export {Buscador};