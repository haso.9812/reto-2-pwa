import React from "react";
import './Boton.css';

function Boton(props){

    const onClickButton = () => {
        props.setOpenModal(prevState => !prevState);
    };

    return(
        <button 
        className="Boton"
        onClick={onClickButton}>
            <span>
            <img alt="" src="https://img.icons8.com/external-flatart-icons-outline-flatarticons/64/ffffff/external-add-basic-ui-elements-flatart-icons-outline-flatarticons.png"/>
            </span>
        </button>
    );
}

export {Boton};